import { InjectionToken } from "@angular/core";
import { UsuarioService } from "./usuario.service";

export const USUARIO_SERVICE = new InjectionToken<UsuarioService>('mytoken',
  { factory: () => new UsuarioService() }
);
