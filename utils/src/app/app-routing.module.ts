import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitialModule } from './initial/initial.module';

const routes: Routes = [
  {
    path: '',
    component: InitialModule,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
