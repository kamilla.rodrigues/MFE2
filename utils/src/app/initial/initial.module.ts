import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioService } from '../services/usuario.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [
    {
      provide: 'USUARIO_SERVICE',
      useClass: UsuarioService
    }
  ],
})
export class InitialModule { }
