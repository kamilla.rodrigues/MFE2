import { loadRemoteModule } from '@angular-architects/module-federation';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { SobreComponent } from './pages/sobre/sobre.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'sobre', component: SobreComponent },
  { path: 'dashboard', loadChildren: () => import('dashboard/Module').then(m => m.DashboardModule) },
  { path: 'auth', loadChildren: () => import('auth/Module').then(m => m.AutenticacaoModule) },
  //{ path: 'utils', loadChildren: () => import('utils/Module').then(m => m.Utils) },
  // {
  //   path: 'teste', loadChildren: () => loadRemoteModule({
  //     type: 'manifest',
  //     remoteName: 'auth',
  //     exposedModule: './AuthModule',
  //   }).then((m) => m.AuthModule),
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
