import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardServiceService } from './services/dashboard-service.service';

// const routes: Routes = [
//   { path: '', component: HomeComponent, pathMatch: 'full' }
// ];

@NgModule({
  declarations: [HomeComponent],
  //exports: [RouterModule],
  providers: [
    {
      provide: 'DASHBOARD_SERVICE',
      useClass: DashboardServiceService
    }
  ],
  imports: [
    CommonModule,
    //[RouterModule.forChild(routes)]
  ]
})
export class DashboardModule { }
