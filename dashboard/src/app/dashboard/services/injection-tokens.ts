import { InjectionToken } from "@angular/core";
import { DashboardServiceService } from "./dashboard-service.service";

interface TokenInjection { }

export const DASHBOARD_SERVICE = new InjectionToken<DashboardServiceService>('mytoken',
  { factory: () => new DashboardServiceService });
