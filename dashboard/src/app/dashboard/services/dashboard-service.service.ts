import { Injectable } from '@angular/core';

@Injectable()
export class DashboardServiceService {

  constructor() { }

  public somar(valor1: number, valor2: number) {
    return valor1 + valor2;
  }
}
