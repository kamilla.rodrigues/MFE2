import { Component, OnInit } from '@angular/core';
import { LoadedServiceInterface } from '../services/loaded-services-interface';
import { RemoteResourcesBuilder } from '../services/remote-resources-builder.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private remoteServices: Array<LoadedServiceInterface> = [];


  constructor(private remoteResourcesBuilder: RemoteResourcesBuilder) {
    this.remoteResourcesBuilder.getServices().subscribe(
      {
        next: (v: any[]) => (this.remoteServices = v),
        error: (e) => console.error(e),
        complete: () => console.info('Service loaded'),
      }
    );
  }

  testeService() {
    console.log('serviços disponiveis', this.remoteServices.map((s) => s.serviceToken));
  }
  ngOnInit(): void { }

}
