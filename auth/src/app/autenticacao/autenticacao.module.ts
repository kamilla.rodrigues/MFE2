import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './component/login/login.component';
import { LoginService } from './services/login.service';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [HomeComponent, LoginComponent],
  providers: [LoginService],
  exports: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    [RouterModule.forChild(routes)]
  ]
})
export class AutenticacaoModule { }
