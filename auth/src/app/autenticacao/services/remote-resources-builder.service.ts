import { Injectable, Injector } from '@angular/core';
import { LoadRemoteModuleOptions, loadRemoteModule } from '@angular-architects/module-federation';
import { Observable, of } from 'rxjs';
import { LoadedServiceInterface } from './loaded-services-interface';


@Injectable({ providedIn: 'root' })
export class RemoteResourcesBuilder {
  private services: Array<LoadedServiceInterface> = [];

  constructor() {
    this.fetchModule(
      {
        type: 'module',
        remoteEntry: 'http://localhost:4200/remoteEntry.js',
        exposedModule: './Dashboard'
      }
    );
  }

  private async fetchModule(moduleOption: LoadRemoteModuleOptions) {
    const module = await Promise.all([loadRemoteModule(moduleOption)]);
    const moduleProviders = module[0].InitialModule.einj.providers;
    const injector = Injector.create({ providers: [moduleProviders] });

    for (const iterator of moduleProviders) {
      const clazz = iterator.useClass;
      this.services.push(
        {
          serviceToken: iterator.provide,
          service: injector.get<typeof clazz>(iterator.provide),
        }
      );
    }
  }

  public getServices(): Observable<any> {
    return of(this.services);
  }
}
