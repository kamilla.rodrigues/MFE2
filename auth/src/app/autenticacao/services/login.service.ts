import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Usuario } from '../shared/models/usuario.model';
import { Login } from '../shared/models/login.model';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  public LS_CHAVE: string = "usuarioLogado";
  constructor() {

  }

  public get usuarioLogado(): Usuario {
    let usuario = localStorage[this.LS_CHAVE];
    return (usuario ? JSON.parse(localStorage[this.LS_CHAVE]) : null);
  }

  public set usuarioLogado(usuario: Usuario) {
    localStorage[this.LS_CHAVE] = JSON.stringify(usuario);
  }

  public login(login: Login): Observable<Usuario | null> {
    let usuario = new Usuario(1, "Razer-Func", login.login, login.senha, "FUNC");

    if (login.login == login.senha) {
      if (login.login == "ADMIN") usuario.perfil = "ADMIN";
      if (login.login == "GERENTE") usuario.perfil = "GERENTE";
      return of(usuario);
    }
    return of(null);
  }

  public logout() {
    delete localStorage[this.LS_CHAVE];
  }

  public soma(valor1: number, valor2: number) {
    return valor1 + valor2;
  }
}
