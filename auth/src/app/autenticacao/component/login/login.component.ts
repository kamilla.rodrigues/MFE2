import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Login } from '../../shared/models/login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  @ViewChild('formLogin') formLogin!: NgForm;
  login: Login = new Login();

  constructor(private loginService: LoginService) {
    if (this.loginService.usuarioLogado) {
      console.log("logado");
    }

  }

  public loginComponente() {
    console.log('login do componente');
  }
}
