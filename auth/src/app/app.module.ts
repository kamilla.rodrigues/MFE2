import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutenticacaoModule } from './autenticacao/autenticacao.module';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/services/auth.service';
//import { InitialModule } from './initial/initial.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //InitialModule
  ],
  exports: [AutenticacaoModule, AuthModule],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
