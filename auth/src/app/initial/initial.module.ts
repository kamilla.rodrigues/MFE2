import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InitialComponent } from './initial.component';



@NgModule({
  declarations: [
    InitialComponent
  ],
  imports: [
    CommonModule
  ]
})
export class InitialModule { }
