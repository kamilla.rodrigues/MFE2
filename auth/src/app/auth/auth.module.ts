import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth.service';
import { BlankPageComponent } from './blank-page/blank-page.component';



@NgModule({
  declarations: [
    BlankPageComponent
  ],
  imports: [CommonModule],
  providers: [AuthService]
})
export class AuthModule { }
