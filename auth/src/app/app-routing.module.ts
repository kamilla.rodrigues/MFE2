import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './autenticacao/home/home.component';
import { BlankPageComponent } from './auth/blank-page/blank-page.component';

const routes: Routes = [
  { path: 'teste', component: HomeComponent, pathMatch: 'full' },
  { path: '', component: BlankPageComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
