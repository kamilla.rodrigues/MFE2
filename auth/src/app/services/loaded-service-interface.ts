export interface LoadedServiceInterface {
  serviceToken: string;
  service: any;
  serviceProperties?: string[];
  serviceMethods?: string[];
}
